import React, { useState, useEffect } from 'react';
import { service } from './service'

export function RepoList({username, api = service}) {
  const [repos, setRepos] = useState([])

  useEffect(() => {
    api
      .getRepos(username)
      .then(repos => setRepos(repos))
  }, [])

  return <>
    <h1>Repos</h1>
    <ul>
      {repos.map(repo => <li key={repo.id}>{repo.name}</li>)}
    </ul>
  </>
}


function App() {
  return (
    <RepoList username="ortense" />
  );
}

export default App;
