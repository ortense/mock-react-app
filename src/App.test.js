// import React from 'react';
// import { render } from '@testing-library/react';
// import App from './App';

// test('renders learn react link', () => {
//   const { getByText } = render(<App />);
//   const linkElement = getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

import React from 'react';
import { RepoList } from './App'
import { render } from '@testing-library/react';


const fakeService = {
  getRepos() {
    return Promise.resolve([{ id: 999, name: 'teste' }])
  }
}

test('Should be cool (:', () => {
  const element = render(<RepoList api={fakeService} />)
  const itemElement = element.getByText(/teste/)
  expect(itemElement).toBeInTheDocument()
})